<?php

/**
* Plugin Name: contact-App
* Plugin URI: https://www.webrins.com/
* Description: This is the very first plugin I ever created.
* Version: 1.0
* Author: Sachin Raj
* Author URI: https://webrins.com/
**/


defined( 'ABSPATH' ) or die('You are in the wrong place !! Cannot have access !!');

/**
 * 
 */


class ContactApp {





/************************************

Commands on activating the plugin

*************************************/

function activate(){

/* Creating a new database table called contact_app_register */
		
		global $wpdb;
		$table_name = $wpdb->prefix . "contact_app_register";
		
		$charset_collate = $wpdb->get_charset_collate();

		if ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_name}'" ) != $table_name ) {

		    $sql = "CREATE TABLE $table_name (
		            id mediumint(9) NOT NULL AUTO_INCREMENT,
		            `fname` text NOT NULL,
		            `lname` text NOT NULL,
		            `email` text NOT NULL
		            PRIMARY KEY  (id)
		    )    $charset_collate;";

		    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		    dbDelta( $sql );
		    
		}

		flush_rewrite_rules();


	}

}


if ( class_exists('ContactApp') ){
	$conApp = new ContactApp('APP INITIALIZED');
	
}

register_activation_hook( __FILE__, array( $conApp, 'activate' ));

 /* ----------------------
/		Api Area 	 	/
--------------------- */
/*
function call_back_name(){
$your_key = 'nRWPrGpL1V9Do7AunkLZKpsYsy7LDILf';
$api_url='https://api.fullcontact.com/v2/person.json?email=bart@fullcontact.com';

$url = 'curl -H"X-FullContact-APIKey:nRWPrGpL1V9Do7AunkLZKpsYsy7LDILf"." https://api.fullcontact.com/v2/person.json?email=bart@fullcontact.com"';

echo $url;
$arguments =  array(
	'method' => 'GET', 
	);

$response = wp_remote_get( $url, $arguments);

if (is_wp_error( $response )) {
	$err_message = $response->get_error_message();
	echo "Something wrong: $err_message";
}

echo '<pre>';
var_dump(wp_remote_retrieve_body($response));
echo '</pre>';
}

*/

 /* ----------------------
/		Create Menu 	/
--------------------- */

add_action('admin_menu', 'plugin_menu_setup');
 
function plugin_menu_setup(){
       
        
         add_menu_page('My Page Title', 'Register Contacts', 'manage_options', 'contactapp', 'CP_register_form' );
			add_submenu_page('contactapp', 'List', 'List Contacts', 'manage_options', 'list-cp-contact', 'registered_data');
			add_submenu_page('contactapp', 'Search', 'Search Contacts', 'manage_options', 'search-cp-contacts', 'search_data' );
}
 

/*Including form to add Contacts*/

function CP_form(){
        include_once('CP_register_form.php');
        cp_register_data();
}

/*Including form to  List contacts*/

function registered_data(){
	include('CP_get_data_form.php');
	get_register_data();
}

/*Including form to search and List contacts*/

function search_data(){
	include('CP_search_data.php');
	get_search_data();
}













/*Styles*/

add_action('admin_head', 'admin_styles');

function admin_styles() {
    echo '<style>
       h2{
			text-align: center;
		}

		.contact-form{
		    display: block;
		    width: 60%;
		    margin: 25px 20px auto;
		    background-color: #ffffff;
		    padding: 10px;
		    border: 1px solid #e8e8e7;
		    border-radius: 5px;
		}
		.contact-form h2{
			text-align: center;
		    background-color: #fafafa;
		    padding: 20px;
		    color: #333333;
		    font-size: 18px;
		}
		form{
			display: block;
		    margin: 0 auto;
		    text-align: center;
		}
		.form-group{
			padding: 20px;
		}
		.form-group label{
			font-size: 14px;
		    font-weight: 700;
		    padding-right: 60px;
		}
		button{
			border: 1px solid #e8e8e8;
		    padding: 7px 56px;
		    font-size: 18px;
		    font-weight: 600;
		    background-color:#fafafa;
		}
		button:hover{
			border: 1px solid #e8e8e8;
		    padding: 7px 56px;
		    font-size: 18px;
		    font-weight: 600;
		    color:#ffffff;
		    background-color:#000000;
		}
		input[type=date], input[type=datetime-local], input[type=datetime], input[type=email], input[type=month], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week]{
			    border: 1px solid #e8e8e8;
		}
		table{
			padding: 20px;
    		border: 1px solid #e8e8e8;
		}
		th{
			width: 130px;
		}
    </style>';
}